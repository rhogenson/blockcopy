module Cache (Cache, new, read, write) where

import qualified System.Posix.Types as T
import qualified Data.IORef as IOR
import qualified Data.Set as S
import qualified Data.Map as M

data WetCache k v = WC
  { cTimeStamps :: S.Set (T.EpochTime, k)
  , cByKey :: M.Map k (T.EpochTime, v)
  , cDirty :: S.Set k
  }

data Cache k v = Cache
  { cWetCache :: IOR.IORef (WetCache k v)
  , cRead :: k -> IO v
  , cWrite :: k -> v -> IO ()
  , cMaxSize :: Int
  }

new :: (k -> IO v) -> (k -> v -> IO ()) -> Int -> IO Cache
new r w size = Cache <$> IOR.newIORef CI
  { cTimeStamps=S.empty, cByKey=M.empty, cDirty=S.empty, cRead=r, cWrite=w
  , cMaxSize=size }

readMiss :: Cache k v -> k -> IO v
readMiss r k =

read :: Cache k v -> k -> IO v
read r k = do
  CI { cByKey, .. } <- IOR.readIORef r
  case M.lookup k cByKey of
    (_, v) -> return v
    Nothing -> readMiss r k
