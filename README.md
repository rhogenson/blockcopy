# blockcopy
This program emulates a filesystem with only one file, which it writes in
blocks into a directory. The use case is to mount an encrypted volume on Google
Drive. I bet it'd be really fast with goofys.
